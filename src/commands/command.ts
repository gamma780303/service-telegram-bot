export enum BotCommand {
    Start = '/start',
    StartSearchById = '/startSearchById',
    GetObjectById = '/getObjectById'
}

export enum BotScene {
    GetObjectById = 'SceneGetObjectById'
}
