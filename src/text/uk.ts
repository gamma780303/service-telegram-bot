export enum Uk {
    Greeting= 'Привіт! Цей бот допоможе вам з пошуком інформації у базі даних SIMBAD. Тисніть кнопку і виконуйте подальші вказівки',
    IdSearch = 'Пошук за ідентифікатором',
    IdSearchStart = 'Введіть ідентифікатор об\'єкта, наприклад TRAPPIST-1',
    IdSearchStarted = 'Йде пошук за ідентифікатором...',
    'Coordinates(ICRS,ep=J2000,eq=2000)' = 'Координати ICRS',
    'Coordinates(FK4,ep=B1950,eq=1950)' = 'Координати FK4',
    'Coordinates(Gal,ep=J2000,eq=2000)' = 'Координати Gal',
    'hierarchy counts' = 'Ієрархія (кількість)',
    'Proper motions' = 'Власний рух',
    'Parallax' = 'Паралакс',
    'Radial Velocity' = 'Радіальна швидкість',
    'Redshift' = 'Червоне зміщення',
    'cz' = 'cz',
    'Flux B' = 'Світловий потік B',
    'Flux V' = 'Світловий потік видимий',
    'Flux G' = 'Світловий потік G',
    'Flux J' = 'Світловий потік J',
    'Flux H' = 'Світловий потік H',
    'Flux K' = 'Світловий потік K',
     Flux = 'Світловий потік',
    'Spectral type' = 'Спектральний тип',
    'Identifiers' = 'Ідентифікатори',
}
